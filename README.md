Réinitialisation Version Evaluation Intellij ideaIU-2020.2.2.win - ultimate edition


Avant de commencer, vérifier les points suivants :
-	S’assurer que le répertoire suivant existe :
C:\Users\<Votre_ID_BNP>\AppData\Roaming\JetBrains\IntelliJIdea2020.2

-	Fermer toutes les instances de Intellij

1 - Création du fichier reset-trial.bat

@echo off
cd C:\Users\<Votre_ID_BNP>\AppData\Roaming\JetBrains\IntelliJIdea2020.2\config\options
type C:\Users\<Votre_ID_BNP>\AppData\Roaming\JetBrains\IntelliJIdea2020.2\other.xml | findstr /v "evlsprt" > C:\Users\<Votre_ID_BNP>\AppData\Roaming\JetBrains\IntelliJIdea2020.2\other.xml
del C:\Users\<Votre_ID_BNP>\AppData\Roaming\JetBrains\IntelliJIdea2020.2\config\eval\*.evaluation.key
reg delete HKEY_CURRENT_USER\Software\JavaSoft\Prefs\jetbrains\idea /f

Vous pouvez le sauvegarder n’importe où car on utilise des chemins absolus dans le fichier.
2 - Suppression de la clé d’évaluation

Dans le répertoire suivant :
C:\Users\<Votre_ID_BNP>\AppData\Roaming\JetBrains\IntelliJIdea2020.2\eval
Supprimer la clé d’evaluation   ******.evaluation.key (suppression du fichier)

3 - Suppression dans le fichier other.xml

Dans le fichier suivant : 
C:\Users\<Votre_ID_BNP>\AppData\Roaming\JetBrains\IntelliJIdea2020.2\options\other.xml
Supprimer toutes les lignes du fichier xml qui contiennent :
<property name="evlsprt………..

 

4 - Exécution du reset-trial.bat

-	Lancer le fichier en utilisant une invite de commandes Windows.
 
-	Lancer Intellij et choisir l’option d’évaluation gratuite.
 






